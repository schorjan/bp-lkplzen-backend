'use strict';

/**
 *  public-archery controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::public-archery.public-archery');
