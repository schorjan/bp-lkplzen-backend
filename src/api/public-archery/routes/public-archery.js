'use strict';

/**
 * public-archery router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::public-archery.public-archery');
