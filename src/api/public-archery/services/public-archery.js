'use strict';

/**
 * public-archery service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::public-archery.public-archery');
