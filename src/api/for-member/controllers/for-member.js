'use strict';

/**
 *  for-member controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::for-member.for-member');
