'use strict';

/**
 * for-member service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::for-member.for-member');
