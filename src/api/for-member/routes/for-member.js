'use strict';

/**
 * for-member router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::for-member.for-member');
