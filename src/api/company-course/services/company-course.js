'use strict';

/**
 * company-course service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::company-course.company-course');
