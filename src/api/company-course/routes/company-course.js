'use strict';

/**
 * company-course router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::company-course.company-course');
