'use strict';

/**
 *  company-course controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::company-course.company-course');
