'use strict';

/**
 *  league-team controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::league-team.league-team');
