'use strict';

/**
 * league-team service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::league-team.league-team');
