'use strict';

/**
 * league-team router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::league-team.league-team');
