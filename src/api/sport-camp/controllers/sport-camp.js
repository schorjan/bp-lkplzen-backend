'use strict';

/**
 *  sport-camp controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::sport-camp.sport-camp');
