'use strict';

/**
 * sport-camp service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::sport-camp.sport-camp');
