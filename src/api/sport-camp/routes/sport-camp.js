'use strict';

/**
 * sport-camp router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::sport-camp.sport-camp');
