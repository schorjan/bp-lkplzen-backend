'use strict';

/**
 * start-membership service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::start-membership.start-membership');
