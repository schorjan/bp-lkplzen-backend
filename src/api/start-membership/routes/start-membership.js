'use strict';

/**
 * start-membership router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::start-membership.start-membership');
