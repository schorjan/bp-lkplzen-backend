'use strict';

/**
 *  start-membership controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::start-membership.start-membership');
