module.exports = {
  routes: [
    {
      method: "POST",
      path: "/message/public-archery",
      handler: "message.public",
    },
    {
      method: "POST",
      path: "/message/start-course",
      handler: "message.start",
    },
    {
      method: "POST",
      path: "/message/contact",
      handler: "message.contact",
    },
  ],
};
