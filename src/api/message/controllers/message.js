const checkValidation = (data) => {
  if (
    !data.name ||
    !data.age ||
    data.age < 6 ||
    data.age > 120 ||
    !data.email ||
    !data.email.includes("@")
  ) {
    return false;
  } else {
    return true;
  }
};

const dateValidation = (date, time) => {
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  if (date <= today || (time != 15 && time != 16 && time != 17)) {
    return false;
  } else {
    return true;
  }
};

const sendMails = async function (customerOptions, adminOptions) {
  await strapi.plugins["email"].services.email.send(customerOptions);
  console.log("email send to customer");

  await strapi.plugins["email"].services.email.send(adminOptions);
  console.log("email send to admin");
};

module.exports = {
  async public(ctx) {
    //console.log(ctx.request.body);
    const message = ctx.request.body;
    const user = message.data;

    if (checkValidation(user)) {
      const tmpDate = new Date(user.date);
      const date = tmpDate.toLocaleDateString("cs-CZ");

      if (!dateValidation(tmpDate, user.time)) {
        ctx.response.status = 400;
        ctx.response.message = "Wrong date and time";
      } else {
        const course = await strapi
          .service("api::public-archery.public-archery")
          .find({ populate: { contact: true } });
        const adminEmail = course.contact.contactEmail;

        const customerOptions = {
          to: user.email,
          subject: "Lekce pro veřejnost",
          text: `Děkujeme za Váš zájem o lekci lukostřelby.
            Vaše zpráva byla odeslána a následně Vás bude kontaktovat trenér s více informacemi.
            
            Zpráva:
            Jméno a příjmení zájemce: ${user.name}
            Kontaktní e-mail: ${user.email}
            Telefonní číslo: ${user.phone}
            Vybrané datum a čas: ${date} v ${user.time} hodin

            Tým 1. LK Plzeň

            Toto je automaticky vygenerovaný e-mail. Pokud chcete svoji účast zrušit, kontaktujte nás na ${adminEmail}.
            `,
          html: `<h1>Děkujeme za Váš zájem o lekci lukostřelby.</h1>
          <p>Vaše zpráva byla odeslána a následně Vás bude kontaktovat trenér s více informacemi.</p>
          <br />
          <br />
          <p><b>Zpráva:</b></p>
          <p><i>Jméno a příjmení zájemce:</i>   ${user.name}</p>
          <p><i>Kontaktní e-mail:</i>   ${user.email}</p>
          <p><i>Telefonní číslo:</i>   ${
            user.phone === "" ? "nevyplněno" : user.phone
          }</p>
          <p><i>Vybrané datum a čas:</i>   ${date} v ${user.time} hodin</p>
          <br />
          <br />
          <p>Tým 1. LK Plzeň</p>
          <br />
          <p><small>Toto je automaticky vygenerovaný e-mail. Pokud chcete svoji účast zrušit, kontaktujte nás na <a href="mailto:${adminEmail}">${adminEmail}</a>.</small></p>`,
        };

        const adminOptions = {
          to: adminEmail,
          subject: "Zájemce - Lukostřelba pro veřejnost",
          text: `Zájemce o lekci lukostřelby dne ${date} v ${user.time} hodin
          
          Jméno a příjmení zájemce: ${user.name}
          Věk zájemce: ${user.age} let
          Jméno a příjmení zákonného zástupce: ${user.parent}
          Kontaktní e-mail: ${user.email}
          Telefonní číslo: ${user.phone}
          Vybrané datum a čas: ${date} v ${user.time} hodin
          Zpráva: ${user.message}

          Prosím odpověz zájemcovi na přiložený email ${user.email} co nejdříve. Děkuji.

          Automaticky generovaný email z webového formuláře.
          `,
          html: `
          <h1>Zájemce o lekci lukostřelby pro veřejnost dne ${date} v ${user.time} hodin</h1>
          <br />
          <p><b>Jméno a příjmení zájemce:</b>  ${user.name}</p>
          <p><b>Věk zájemce:</b> ${user.age} let</p>
          <p><b>Jméno a příjmení zákonného zástupce:</b> ${user.parent}</p>
          <p><b>Kontaktní e-mail:</b> ${user.email}</p>
          <p><b>Telefonní číslo:</b> ${user.phone}</p>
          <p><b>Vybrané datum a čas:</b> ${date} v ${user.time} hodin</p>
          <p><b>Poznámka:</b> ${user.message}</p>
          <br />
          <p><b>Prosím odpověz zájemcovi na přiložený email <a href="${user.email}">${user.email}</a> co nejdříve. Děkuji.</b></p>
          <br />
          <p><small>Automaticky generovaný email z webového formuláře.</small></p>
          `,
        };

        await sendMails(customerOptions, adminOptions);

        ctx.response.status = 200;
      }
    } else {
      ctx.response.status = 400;
      ctx.response.message = "Wrong form data";
    }
  },

  async start(ctx) {
    //console.log(ctx.request.body);
    const message = ctx.request.body;
    const user = message.data;

    if (checkValidation(user)) {
      const course = await strapi
        .service("api::start-course.start-course")
        .find({ populate: { courseTerms: true, contact: true } });

      const term = course.courseTerms.find((term) => {
        return term.id === user.term;
      });

      const adminMail = course.contact.contactEmail;

      if (!term) {
        ctx.response.status = 400;
        ctx.response.message = "Wrong term";
      } else {
        const customerOptions = {
          to: user.email,
          subject: "Kurz lukostřelby pro začínající střelce",
          text: `Děkujeme za Váš zájem o kurz lukostřelby pro začínající střelce.
              Vaše zpráva byla odeslána a následně Vás bude kontaktovat trenér s více informacemi.
              
              Zpráva:
              Jméno a příjmení zájemce: ${user.name}
              Kontaktní e-mail: ${user.email}
              Telefonní číslo: ${user.phone}
              Vybraný kurz - termíny:
                první kurz: ${term.firstTime},
                druhý kurz: ${term.secondTime},
                třetí kurz: ${term.thirdTime},
                čtvrtý kurz: ${term.fourthTime}
  
              Tým 1. LK Plzeň

              Toto je automaticky vygenerovaný e-mail. Pokud chcete svoji účast zrušit, kontaktujte nás na ${adminMail}.
              `,
          html: `<h1>Děkujeme za Váš zájem o kurz lukostřelby pro začínající střelce.</h1>
            <p>Vaše zpráva byla odeslána a následně Vás bude kontaktovat trenér s více informacemi.</p>
            <br />
            <br />
            <p><b>Zpráva:</b></p>
            <p><i>Jméno a příjmení zájemce:</i>   ${user.name}</p>
            <p><i>Kontaktní e-mail:</i>   ${user.email}</p>
            <p><i>Telefonní číslo:</i>   ${
              user.phone === "" ? "nevyplněno" : user.phone
            }</p>
            <p><i>Vybraný kurz - termíny:</i></p>
            <ul>    
              <li><b>první kurz:</b> ${term.firstTime},</li>
              <li><b>druhý kurz:</b> ${term.secondTime},</li>
              <li><b>třetí kurz:</b> ${term.thirdTime},</li>
              <li><b>čtvrtý kurz:</b> ${term.fourthTime}</li>
            </ul>
            <br />
            <br />
            <p>Tým 1. LK Plzeň</p>
            <br />
            <p><small>Toto je automaticky vygenerovaný e-mail. Pokud chcete svoji účast zrušit, kontaktujte nás na <a href="mailto:${adminMail}">${adminMail}</a>.</small></p>`,
        };

        const adminOptions = {
          to: adminMail,
          subject: "Zájemce - Kurz nováčků",
          text: `Zájemce o kurz lukostřelby pro začínající střelce na termín:
            první kurz: ${term.firstTime},
            druhý kurz: ${term.secondTime},
            třetí kurz: ${term.thirdTime},
            čtvrtý kurz: ${term.fourthTime}
            
            Jméno a příjmení zájemce: ${user.name}
            Věk zájemce: ${user.age} let
            Jméno a příjmení zákonného zástupce: ${user.parent}
            Kontaktní e-mail: ${user.email}
            Telefonní číslo: ${user.phone}
            Zpráva: ${user.message}
  
            Prosím odpověz zájemcovi na přiložený email ${user.email} co nejdříve. Děkuji.
  
            Automaticky generovaný email z webového formuláře.
            `,
          html: `
            <h1>Zájemce o kurz lukostřelby pro začínající střelce na termín:</h1>
            <ul>    
            <li><b>první kurz:</b> ${term.firstTime},</li>
            <li><b>druhý kurz:</b> ${term.secondTime},</li>
            <li><b>třetí kurz:</b> ${term.thirdTime},</li>
            <li><b>čtvrtý kurz:</b> ${term.fourthTime}</li>
            </ul>
            <br />
            <p><b>Jméno a příjmení zájemce:</b>  ${user.name}</p>
            <p><b>Věk zájemce:</b> ${user.age} let</p>
            <p><b>Jméno a příjmení zákonného zástupce:</b> ${user.parent}</p>
            <p><b>Kontaktní e-mail:</b> ${user.email}</p>
            <p><b>Telefonní číslo:</b> ${user.phone}</p>
            <p><b>Poznámka:</b> ${user.message}</p>
            <br />
            <p><b>Prosím odpověz zájemcovi na přiložený email <a href="${user.email}">${user.email}</a> co nejdříve. Děkuji.</b></p>
            <br />
            <p><small>Automaticky generovaný email z webového formuláře.</small></p>
            `,
        };

        await sendMails(customerOptions, adminOptions);
        ctx.response.status = 200;
      }
    } else {
      ctx.response.status = 400;
      ctx.response.message = "Wrong form data";
    }
  },

  async contact(ctx) {
    //console.log(ctx.request.body);
    const message = ctx.request.body;
    const user = message.data;

    if (checkValidation(user)) {
      const adminEmail = "";

      const customerOptions = {
        to: user.email,
        subject: "1.LK Plzeň 1935 děkuje za zprávu",
        text: `Děkujeme za Váši zprávu.
            Vaše zpráva byla odeslána a následně Vás zkontaktujeme, pokud Vám budeme moci pomoci.
            
            Zpráva:
            Jméno a příjmení: ${user.name}
            Kontaktní e-mail: ${user.email}
            Telefonní číslo: ${user.phone}
            Vaše zpráva: ${user.message}

            Tým 1. LK Plzeň

            Toto je automaticky vygenerovaný e-mail.
            `,
        html: `<h1>Děkujeme za Váši zprávu.</h1>
          <p>Vaše zpráva byla odeslána a následně Vás zkontaktujeme, pokud Vám budeme moci pomoci..</p>
          <br />
          <br />
          <p><b>Zpráva:</b></p>
          <p><i>Jméno a příjmení zájemce:</i>   ${user.name}</p>
          <p><i>Kontaktní e-mail:</i>   ${user.email}</p>
          <p><i>Telefonní číslo:</i>   ${
            user.phone === "" ? "nevyplněno" : user.phone
          }</p>
          <p><i>Vaše zpráva:</i> ${user.message}</p>
          <br />
          <br />
          <p>Tým 1. LK Plzeň</p>
          <br />
          <p><small>Toto je automaticky vygenerovaný e-mail.</small></p>`,
      };

      const adminOptions = {
        to: adminEmail,
        subject: "Dotaz/zpráva z webu",
        text: `Dotaz nebo zpráva zadaná na webu
          
          Jméno a příjmení: ${user.name}
          Kontaktní e-mail: ${user.email}
          Telefonní číslo: ${user.phone}
          Dotaz/zpráva: ${user.message}

          Prosím odpověz osobě na přiložený email ${user.email} co nejdříve. Děkuji.

          Automaticky generovaný email z webového formuláře.
          `,
        html: `
          <h1>Dotaz nebo zpráva zadaná na webu</h1>
          <br />
          <p><b>Jméno a příjmení zájemce:</b>  ${user.name}</p>
          <p><b>Kontaktní e-mail:</b> ${user.email}</p>
          <p><b>Telefonní číslo:</b> ${user.phone}</p>
          <p><b>Dotaz/zpráva:</b> ${user.message}</p>
          <br />
          <p><b>Prosím odpověz osobě na přiložený email <a href="${user.email}">${user.email}</a> co nejdříve. Děkuji.</b></p>
          <br />
          <p><small>Automaticky generovaný email z webového formuláře.</small></p>
          `,
      };

      await sendMails(customerOptions, adminOptions);

      ctx.response.status = 200;
    } else {
      ctx.response.status = 400;
      ctx.response.message = "Wrong form data";
    }
  },
};
