'use strict';

/**
 *  start-course controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::start-course.start-course');
