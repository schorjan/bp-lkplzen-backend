'use strict';

/**
 * start-course router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::start-course.start-course');
