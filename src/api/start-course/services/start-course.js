'use strict';

/**
 * start-course service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::start-course.start-course');
