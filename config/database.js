module.exports = ({ env }) => ({
  connection: {
    client: "postgres",
    connection: {
      host: env("DATABASE_HOST", "127.0.0.1"),
      port: env.int("DATABASE_PORT", 5432),
      database: env("DATABASE_NAME", "lkplzen-db"),
      user: env("DATABASE_USERNAME", "lkplzen"),
      password: env("DATABASE_PASSWORD", "lkplzen1935"),
      //ssl: env.bool('DATABASE_SSL', true),
      ssl: false,
    },
  },
});
